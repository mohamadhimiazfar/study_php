<!DOCTYPE html>
<html>
<body>

<?php
$x = 30;
$y = 10;

echo "X = 30, Y = 10";
echo "<br>";
echo "<br>";
echo "Adding operator" . " = ";
echo $x + $y;
echo "<br>";
echo "Subtract operator" . " = ";
echo $x - $y;
echo "<br>";
echo "Multiplication operator" . " = ";
echo $x * $y;
echo "<br>";
echo "Division operator" . " = ";
echo $x / $y;
echo "<br>";
echo "Modulus operator" . " = ";
echo $x % $y;
echo "<br>";
echo "Exponentiation operator" . " = ";
echo $x ** $y;
?>

</body>
</html>