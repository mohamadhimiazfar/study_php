<!DOCTYPE html>
<html>
<body>

<?php
echo "Today is " . date("y/m/d") . "<br>";
echo "Today is " . date("y.m.d") . "<br>";
echo "Today is " . date("y-m-d") . "<br>";
echo "Today is " . date("l");
?>
<br><br>

© 2010-<?php echo date("Y");?>
<br><br>

<?php
echo "The time is " . date ("h:i:sa");
?>
<br><br>

<?php
date_default_timezone_set("Asia/Singapore");
echo "The time is " . date("h:i:sa");
?>
<br>
<br>
<?php
$d=strtotime("11:59pm October 26 2022");
echo "Created date is " . date("Y-m-d h:i:sa", $d);
?>
<br>
<br>

<?php
$d=strtotime("tomorrow");
echo date("Y-m-d h:i:sa", $d) . "<br>";

$d=strtotime("next Saturday");
echo date("Y-m-d h:i:sa", $d) . "<br>";

$d=strtotime("+3 Months");
echo date("Y-m-d h:i:sa", $d) . "<br>";
?>
<br>
<br>

<?php
$startdate=strtotime("Saturday");
$enddate=strtotime("+6 weeks", $startdate);

while ($startdate < $enddate) {
  echo date("M d", $startdate) . "<br>";
  $startdate = strtotime("+1 week", $startdate);
}
?>

</body>
</html>
